
var numArr = [];

function themSo() {
    console.log("yes");
    var tongSoChan = 0;
    var tongSoAm = 0;
    var number = document.querySelector("#number").value * 1;
    var soLuongSoAm = 0;

    // xoá giá trị cũ sau khi nhập để nhập giá trị mới
    document.querySelector("#number").value ="";

    // thêm giá trị user nhập vào
    numArr.push(number);
    // console.log(numArr);
 
    // duyệt mảng
    // for (var i = 0; i < numArr.length; i++) {
    //     var number = numArr[i];
    //     if (number % 2 == 0) {
    //         tongSoChan += numArr[i];
    //     }
    //     if (number < 0) {
    //         soLuongSoAm++;
    //     }
    // }

    // for (var i = 0; i < numArr.length; i++) {
    //     var number = numArr[i];
    //     if (number < 0) {
    //         tongSoAm += numArr[i];
    //     }
       
    // }


    var contentHTML = `<h3> Mảng = [ ${numArr} ]</h3>`;

    document.querySelector("#result").innerHTML = contentHTML;
}

function tongSoDuong() {
    var tongSoDuong = 0;
    for (var i = 0; i < numArr.length; i++) {
        var number = numArr[i];
        if (number > 0) {
            tongSoDuong += numArr[i];
        }
       
    }
    var contentHTML = `<h3> Tổng số dương = ${tongSoDuong} </h3>`;

    document.querySelector("#TSD").innerHTML = contentHTML;
}

function numberOfPos() {
    var numberOfPos = 0;
    for (var i = 0; i < numArr.length; i++) {
        var number = numArr[i];
        if (number > 0){
            numberOfPos++;
        }
    }
    var contentHTML = `<h3> Có ${numberOfPos} số dương</h3>`;

    document.querySelector("#NOP").innerHTML = contentHTML;
}

function smallestNum() {
    var smallestNum = numArr[0];
    for (var i = 1; i <= numArr.length; i++){
        var number = numArr[i];
        if (number < smallestNum) {
            smallestNum = number;
        }
    }
    var contentHTML = `<h3> Số nhỏ nhất trong mảng là: ${smallestNum} </h3>`;

    document.querySelector("#SNN").innerHTML = contentHTML;
}

function smallestNumPos() {
    var smallestNumPos = numArr[0];
    for (var i = 1; i <= numArr.length; i++) {
        var number = numArr[i];
        if (number > 0 && number < smallestNumPos) {
            smallestNumPos = number;         
        }

    }
    var contentHTML = `<h3> Số dương nhỏ nhất trong mảng là: ${smallestNumPos} </h3>`;

    document.querySelector("#SDNN").innerHTML = contentHTML;
}

function lastEvenNum() {
    // phần tử cuối cừng trong mảng
    var lastEvenNum = numArr[numArr.length - 1];
    for (var i = 0; i < numArr.length; i++) {
        var number = numArr[i];
        if (numArr[0] == 1) {
            lastEvenNum = 0;
        }
        if (number % 2 == 0) {
            lastEvenNum = number;
        }
       
    }

    var contentHTML = `<h3> Số chẵn cuối cùng trong mảng là: ${lastEvenNum} </h3>`;

    document.querySelector("#SCCC").innerHTML = contentHTML;
}

function swap() {
   var pos1 = document.getElementById("pos1").value * 1;
   var pos2 = document.getElementById("pos2").value * 1;
   console.log(pos1, pos2);
   var t = numArr[pos1];
   numArr[pos1] = numArr[pos2];
   numArr[pos2] = t;

   var contentHTML = `<h3> Mảng sau khi đổi: [ ${numArr} ] </h3>`;

    document.querySelector("#swap").innerHTML = contentHTML;
}

function upSort() {
   let c = numArr;
   
   var contentHTML = `<h3> Mảng sau sắp xếp: [ ${c.sort(test)} ] </h3>`;
   
   document.querySelector("#SXTD").innerHTML = contentHTML;
   
}

function test(a, b) {
   
   return a - b;
   
   
}

function countNum() {
    var count = 0;
    for(var i = 0; i < numArr.length; i++){
        if (Number.isInteger(numArr[i]))
            count++;
    }
    var contentHTML = `<h3> Có ${count} số nguyên </h3>`;
   
    document.querySelector("#count").innerHTML = contentHTML;
}

function compareNum() {
    var countPos = 0;
    var countNeg = 0;
    for(var i = 0; i < numArr.length; i++) {
        var number = numArr[i];
        if (number > 0) {
            countPos++;
        }
    }
    for(var i = 0; i < numArr.length; i++) {
        var number = numArr[i];
        if (number < 0) {
            countNeg++;
        }
    }
    if (countPos > countNeg) {
        var contentHTML = `<h3> Số dương > Số âm </h3>`;
   
        document.querySelector("#compare").innerHTML = contentHTML;
    } else {
        var contentHTML = `<h3> Số dương < Số âm </h3>`;
   
        document.querySelector("#compare").innerHTML = contentHTML;
    }
}